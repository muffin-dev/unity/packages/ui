# Muffin Dev for Unity - UI

Icons, styles, and various UI utilities for legacy GUI, UI package and UI Elements.

## Installation

We recommend using the *Package Manager* to install and update this package. This means that you must use *Unity 2018+*, and have [Git](https://git-scm.com/download) installed on your machine.

1. In your Unity project, open the *Package Manager* window from `Window > Package Manager`.
2. Click on the *+* icon in the top-left corner, and select *Add package from git URL...*.

![Add package dropdown menu](./Documentation~/Images/package-manager-add.png)

3. In the text field, enter the URL to this package's repository, and click on the *Add* button.

![Package URL text field](./Documentation~/Images/package-manager-url.png)

4. Wait for Unity to get the files, and you're ready to go!

## Documentation

[=> See full documentation](./Documentation~/README.md)

## License

Muffin Dev Libraries © 2022 by MuffinDev is licensed under Creative Commons Attribution 4.0 International (CC BY 4.0). To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0

Also, in the case of this package, your forks are your own, and can be distributed and used commercially.

![CC BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)