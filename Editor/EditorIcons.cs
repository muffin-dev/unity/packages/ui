/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using UnityEngine;

using MuffinDev.Core.EditorOnly;

namespace MuffinDev.UI.EditorOnly
{

    /// <summary>
    /// Utility for getting icon resources.
    /// </summary>
    public static class EditorIcons
    {

        #region Fields

        private static MaterialIcons s_materialIconsAsset = null;

        #endregion


        #region Public API
        
        /// <summary>
        /// Gets the <see cref="MaterialIcons"/> asset used by this utility.
        /// </summary>
        public static MaterialIcons MaterialIconsAsset
        {
            get
            {
                if (s_materialIconsAsset == null)
                {
                    s_materialIconsAsset = EditorHelpers.FindAssetOfType<MaterialIcons>();
                }
                return s_materialIconsAsset;
            }
        }

        /// <summary>
        /// Gets the named icon.
        /// </summary>
        /// <param name="name">The name of the icon you want to get.</param>
        /// <param name="white">Do you want to get the white version of the icon?</param>
        /// <param name="x2">Do you want the doubled size of the icon?</param>
        /// <returns>Returns the found icon.</returns>
        public static Texture2D GetIcon(string name, bool white = false, bool x2 = false)
        {
            if (MaterialIconsAsset != null)
            {
                return MaterialIconsAsset.GetIcon(name, white, x2);
            }
            return null;
        }

        /// <summary>
        /// Creates a <see cref="GUIContent"/>, using the named icon as content's icon.
        /// </summary>
        /// <param name="iconName">The name of the icon to use for the output <see cref="GUIContent"/>.</param>
        /// <param name="text">The text of the output <see cref="GUIContent"/>.</param>
        /// <param name="tooltip">The tooltip of the output <see cref="GUIContent"/>.</param>
        /// <param name="white">Do you want to get the white version of the icon?</param>
        /// <param name="x2">Do you want the doubled size of the icon?</param>
        /// <returns>Returns the generated <see cref="GUIContent"/>.</returns>
        public static GUIContent IconContent(string iconName, string text, string tooltip, bool white = false, bool x2 = false)
        {
            return new GUIContent(text, GetIcon(iconName, white, x2), tooltip);
        }

        /// <inheritdoc cref="IconContent(string, string, string, bool, bool)"/>
        public static GUIContent IconContent(string iconName, string tooltip, bool white = false, bool x2 = false)
        {
            return new GUIContent(GetIcon(iconName, white, x2), tooltip);
        }

        /// <inheritdoc cref="IconContent(string, string, string, bool, bool)"/>
        public static GUIContent IconContent(string iconName, bool white = false, bool x2 = false)
        {
            return new GUIContent(GetIcon(iconName, white, x2));
        }

        #endregion

    }

}