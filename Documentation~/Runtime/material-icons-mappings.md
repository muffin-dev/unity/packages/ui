# UI - Runtime - `MaterialIconsMappings`

The object representation of a Material Icons mappings JSON file.

Use the [`MaterialIconsMappingsUtility`](./material-icons-mappings-utility.md) to parse the mappings JSON files into this kind of objects.

## Public API

```cs
[System.Serializable]
public class MaterialIconsMappings { }
```

### Fields

#### `mappings`

```cs
public Atlas[] mappings = { };
```

### Subclasses

#### `Atlas`

```cs
[System.Serializable]
public class Atlas
{
  public int iconSize;
  public Icon[] icons = { };
}
```

#### `Icon`

```cs
[System.Serializable]
public class Icon
{
  public int x;
  public int y;
  public string name;
  public string category;
}
```

---

[<= Back to summary](./README.md)