# UI - Documentation - Runtime

## Summary

- [MaterialIcons](./material-icons.md): Contains the references to the Material Icons atlases and mappings.
- [MaterialIconsMappings](./material-icons-mappings.md): The object representation of a Material Icons mappings JSON file.
- [MaterialIconsMappingsUtility](./material-icons-mappings-utility.md): Utility for parsing Material Icons' JSON mappings file.

---

[<= Back to package's summary](../README.md)