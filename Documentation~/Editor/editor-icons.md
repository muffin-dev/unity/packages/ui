# UI - Runtime - `EditorIcons`

Utility for getting icon resources.

For now, it only uses [Material Icons](../Runtime/material-icons.md) as a source for the icons.

You can browse all Material Icons by name or tag from *Google Fonts*: https://fonts.google.com/icons?icon.set=Material+Icons

## Public API

```cs
public static class EditorIcons { }
```

### Functions

#### `GetIcon()`

```cs
public static Texture2D GetIcon(string name, bool white = false, bool x2 = false)
```

Gets the named icon.

- `string name`: The name of the icon you want to get.
- `bool white = false`: Do you want to get the white version of the icon?
- `bool x2 = false`: Do you want the doubled size of the icon?

Returns the found icon.

#### `IconContent()`

```cs
public static GUIContent IconContent(string iconName, bool white = false, bool x2 = false);
public static GUIContent IconContent(string iconName, string tooltip, bool white = false, bool x2 = false);
public static GUIContent IconContent(string iconName, string text, string tooltip, bool white = false, bool x2 = false);
```

Creates a `GUIContent`, using the named icon as content's icon.

- `string iconName`: The name of the icon to use for the output `GUIContent`.
- `string text`: The text of the output `GUIContent`.
- `string tooltip`: The text of the output `GUIContent`.
- `bool white = false`: Do you want to get the white version of the icon?
- `bool x2 = false`: Do you want the doubled size of the icon?

Returns the generated `GUIContent`.

### Properties

#### `MaterialIconsAsset`

```cs
public static MaterialIcons MaterialIconsAsset { get; }
```

Gets the [`MaterialIcons`](../Runtime/material-icons.md) asset used by this utility.

---

[<= Back to summary](./README.md)