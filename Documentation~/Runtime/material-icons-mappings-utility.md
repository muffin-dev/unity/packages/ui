# UI - Runtime - `MaterialIconsMappingsUtility`

Utility for parsing Material Icons' JSON mappings file.

## Public API

```cs
public static class MaterialIconsMappingsUtility { }
```

### Functions

#### `Parse()`

```cs
public static MaterialIconsMappings Parse(string mappingsJSONFilePath);
public static MaterialIconsMappings Parse(TextAsset mappingsJSONFileAsset);
```

Parses the given mappings file.

- `string mappingsJSONFilePath`: The path to the mappings JSON file.
- `TextAsset mappingsJSONFileAsset`: The mappings JSON file asset.

Returns the parsed data.

---

[<= Back to summary](./README.md)