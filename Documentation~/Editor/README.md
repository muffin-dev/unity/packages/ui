# UI - Documentation - Editor

## Summary

- [EditorIcons](./editor-icons.md): Utility for getting icon resources.

---

[<= Back to package's summary](../README.md)