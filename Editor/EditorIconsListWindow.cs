/**
 * Muffin Dev (c) 2022
 * Author: contact@muffindev.com
 */

using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using MuffinDev.Core;
using MuffinDev.Core.EditorOnly;

namespace MuffinDev.UI.EditorOnly
{

    /// <summary>
    /// Displays the list of all available icons.
    /// </summary>
    public class EditorIconsListWindow : EditorWindow
    {

        #region Subclasses

        /// <summary>
        /// Groups informations about a displayed icon.
        /// </summary>
        private struct IconInfo
        {
            public string category;
            public string name;
            public Texture2D icon;
        }

        #endregion


        #region Fields

        private const string WINDOW_TITLE = "Editor Icons List";
        private const string MENU_ITEM = EditorConstants.EDITOR_WINDOW_MENU + "/" + WINDOW_TITLE;

        private const int NB_ICONS_PER_PAGE = 20;
        private const string ALL_CATEGORIES_LABEL = "All";

        private static string[] s_categoriesLabels = null;
        private static string[] s_nicifiedCategoriesLabels = null;

        [SerializeField]
        [Tooltip("Filter icons by names.")]
        private string _searchString = string.Empty;

        [SerializeField]
        [Tooltip("Filter icons by catagories.")]
        private int _selectedCategoryIndex = 0;

        [SerializeField]
        [Tooltip("The current pagination value.")]
        private Pagination _pagination = new Pagination();

        [SerializeField]
        private Vector2 _scrollPosition = Vector2.zero;

        /// <summary>
        /// The list of icons that match with the current filters.
        /// </summary>
        private IconInfo[] _displayedIcons = null;

        #endregion


        #region Public API

        /// <summary>
        /// Opens this editor window.
        /// </summary>
        [MenuItem(MENU_ITEM)]
        public static EditorIconsListWindow Open()
        {
            EditorIconsListWindow window = GetWindow<EditorIconsListWindow>(false, WINDOW_TITLE, true);
            window.Show();
            return window;
        }

        #endregion


        #region UI

        /// <summary>
        /// Draws this window GUI on screen.
        /// </summary>
        private void OnGUI()
        {
            EditorGUI.BeginChangeCheck();
            _selectedCategoryIndex = EditorGUILayout.Popup("Category", _selectedCategoryIndex, NicifiedCategoriesLabels);
            _searchString = EditorGUILayout.TextField("Search", _searchString);
            if (EditorGUI.EndChangeCheck())
                ReloadIcons();

            MoreEditorGUI.PaginationField(ref _pagination);

            EditorGUILayout.Space();
            MoreEditorGUI.HorizontalSeparator();
            EditorGUILayout.Space();

            _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);
            {
                foreach (IconInfo i in _pagination.Paginate(DisplayedIcons))
                {
                    using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                    {
                        using (new EditorGUILayout.HorizontalScope())
                        {
                            Rect rect = EditorGUILayout.GetControlRect(GUILayout.Width(i.icon.width), GUILayout.Height(i.icon.height));
                            EditorGUI.DrawTextureTransparent(rect, i.icon);

                            using (new EditorGUILayout.VerticalScope())
                            {
                                EditorGUILayout.LabelField(i.name, EditorStyles.boldLabel);
                                EditorGUILayout.LabelField(i.category, EditorStyles.largeLabel.FontStyle(FontStyle.Italic).FontSize(10));
                            }
                        }
                    }
                }
            }
            EditorGUILayout.EndScrollView();
        }

        #endregion


        #region Private API

        /// <inheritdoc cref="s_categoriesLabels"/>
        private static string[] CategoriesLabels
        {
            get
            {
                if (s_categoriesLabels == null)
                {
                    List<string> categoriesLabelsList = new List<string>();

                    categoriesLabelsList.Add(ALL_CATEGORIES_LABEL);
                    categoriesLabelsList.AddRange(EditorIcons.MaterialIconsAsset.IconCategories);

                    s_categoriesLabels = categoriesLabelsList.ToArray();
                }
                return s_categoriesLabels;
            }
        }

        /// <inheritdoc cref="s_nicifiedCategoriesLabels"/>
        private static string[] NicifiedCategoriesLabels
        {
            get
            {
                if (s_nicifiedCategoriesLabels == null)
                {
                    List<string> nicifiedCategoriesLabelsList = new List<string>();
                    foreach (string cat in CategoriesLabels)
                        nicifiedCategoriesLabelsList.Add(ObjectNames.NicifyVariableName(cat));
                    s_nicifiedCategoriesLabels = nicifiedCategoriesLabelsList.ToArray();
                }
                return s_nicifiedCategoriesLabels;
            }
        }

        /// <inheritdoc cref="_displayedIcons"/>
        private IconInfo[] DisplayedIcons
        {
            get
            {
                if (_displayedIcons == null)
                    ReloadIcons();
                return _displayedIcons;
            }
        }

        /// <summary>
        /// Reloads the list of icons to display, based on the active filters and pagination value.
        /// </summary>
        private void ReloadIcons()
        {
            List<string> iconNames = new List<string>();
            if (_selectedCategoryIndex > 0)
                iconNames.AddRange(EditorIcons.MaterialIconsAsset.GetIconNamesInCategory(CategoriesLabels[_selectedCategoryIndex]));
            else
                iconNames.AddRange(EditorIcons.MaterialIconsAsset.IconNames);

            if (!string.IsNullOrEmpty(_searchString))
            {
                string search = _searchString.ToLower().Trim();
                iconNames.RemoveAll(name => !name.ToLower().Contains(search));
            }

            List<IconInfo> iconInfos = new List<IconInfo>();
            foreach (string iconName in iconNames)
            {
                iconInfos.Add(new IconInfo
                {
                    name = iconName,
                    category = EditorIcons.MaterialIconsAsset.GetIconCategoryName(iconName),
                    icon = EditorIcons.MaterialIconsAsset.GetIcon(iconName, true, true)
                });
            }

            _displayedIcons = iconInfos.ToArray();
            _pagination = new Pagination(0, NB_ICONS_PER_PAGE, _displayedIcons.Length);
        }

        #endregion

    }

}