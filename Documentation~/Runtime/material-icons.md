# UI - Runtime - `MaterialIcons`

Contains the references to the Material Icons atlases and mappings, so you can get these icons easily.

In the editor context, prefer using the [`EditorIcons`](../Editor/editor-icons.md) utility instead of using this asset directly.

Also, you can browse all Material Icons by name or tag from *Google Fonts*: https://fonts.google.com/icons?icon.set=Material+Icons

## Usage

The following example creates a menu in `Demo > Material Icons` in which you can type the name of an icon to see how it looks.

```cs
using UnityEngine;
using UnityEditor;
using MuffinDev.UI;
public class DemoMaterialIcons : EditorWindow
{
  [SerializeField]
  private MaterialIcons _materialIconsAsset = null;

  [SerializeField]
  private string _iconName = "account_circle";

  [MenuItem("Demos/Material Icons")]
  public static void ShowWindow()
  {
    DemoMaterialIcons window = GetWindow<DemoMaterialIcons>(false, "Demo Material Icons", true);
    window.Show();
  }

  private void OnGUI()
  {
    _materialIconsAsset = EditorGUILayout.ObjectField("Material Icons Asset", _materialIconsAsset, typeof(MaterialIcons), false) as MaterialIcons;

    GUI.enabled = _materialIconsAsset != null;
    _iconName = EditorGUILayout.TextField("Icon Name", _iconName);
    GUI.enabled = true;

    if (_materialIconsAsset != null)
    {
      Texture2D icon = _materialIconsAsset.GetIcon(_iconName, true, true);
      if (icon != null)
      {
        Rect rect = EditorGUILayout.GetControlRect(false, GUILayout.Height(_materialIconsAsset.IconSize2x));
        EditorGUI.DrawTextureTransparent(rect, icon, ScaleMode.ScaleToFit);
      }
      else
      {
        EditorGUILayout.HelpBox("Please enter a valid icon name...", MessageType.Warning);
      }
    }

    if (GUILayout.Button("Browse Material Icons..."))
    {
      Application.OpenURL(MaterialIcons.MATERIAL_ICONS_URL);
    }
  }
}
```

![Material Icons demo window preview](../Images/material-icons-demo.png)

## Public API

```cs
public class MaterialIcons : ScriptableObject { }
```

### Constants

#### `MATERIAL_ICONS_URL`

```cs
public const string MATERIAL_ICONS_URL = "https://fonts.google.com/icons?icon.set=Material+Icons";
```

External URL to the Material Icons browser from *Google Fonts*.

### Functions

#### `GetIcon()`

```cs
public Texture2D GetIcon(string name, bool white = false, bool x2 = false)
```

Gets a Material Icon by its name.

- `string name`: The name of the icon you want to get.
- `bool white = false`: Do you want to get the white version of the icon?
- `bool x2 = false`: Do you want the doubled size of the icon?

Returns the found icon.

#### `GetIconNamesInCategory()`

```cs
public string[] GetIconNamesInCategory(string categoryName)
```

Gets all the icon names for the named category.

- `string categoryName`: The name of the category of which you want to get the icon names.

Returns the found icons names.

#### `GetIconCategoryName()`

```cs
public string GetIconCategoryName(string iconName)
```

Gets the named icon's category.

- `string iconName`: The name of the icon of which you want to get the category name.

Returns the found category name.

### Properties

#### `IconSize`

```cs
public int IconSize { get; }
```

The size (in pixels) of the icons at regular size.

#### `IconSize2x`

```cs
public int IconSize2x { get; }
```

The size (in pixels) of the icons at doubled size.

#### `IconCategories`

```cs
public string[] IconCategories { get; }
```

Gets the list of all icon categories.

#### `IconNames`

```cs
public string[] IconNames { get; }
```

Gets all the icons names.

---

[<= Back to summary](./README.md)